<!DOCTYPE HTML>
<html lang="pt-BR">

<head>

    <title>Needlist - Monte listas para qualquer evento</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="description" content="Olá, essa é uma lista de desejos. Ela surgiu da vontade de facilitar a escolha de algo para demonstrar seu afeto por alguém e como forma de proteger o meio ambiente, pois tudo que se encontra na lista será usufruido." />
    <meta name="keywords" content="lista, evento, presente" />
    <meta name="author" content="Needlist" />

    <meta property="og:title" content="Needlist - Monte listas para qualquer evento">
    <meta property="og:url" content="http://www.needlist.com.br/">
    <meta property="og:description" content="Olá, essa é uma lista de desejos. Ela surgiu da vontade de facilitar a escolha de algo para demonstrar seu afeto por alguém e como forma de proteger o meio ambiente, pois tudo que se encontra na lista será usufruido.">
    <meta property="og:type" content="article">
    <meta property="og:locale" content="pt_BR">
    <meta property="og:image" content="http://www.needlist.com.br/img/capa-site.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="581">
    <meta property="og:image:height" content="300">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="shortcut icon" href="assets/img/favicon.png" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,900|Playfair+Display:400i,900i" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href='assets/css/grid-ramon.css' />
    <link rel="stylesheet" type="text/css" href='assets/css/estilo.css' />

</head>

<body>

    <nav class="fixed nav-act">

        <div class="max-w">

            <ul class="col-100">

                <li class="float-left"><a href="index.php" class="logo scroll">needlist</a></li>
                <li class="float-right"><a href="index.php" class="menu-item scroll">Criar lista</a></li>

            </ul>

        </div>

    </nav>

    <section class="interna center bg-img" style="background-image: url(assets/img/bg-v.png);">
        
        <div class="bg-b-90">

            <main class="color-w">

                <h1>Monte listas para qualquer evento</h1>
                <p>Olá, essa é uma lista de desejos. Ela surgiu da vontade de facilitar a escolha de algo para demonstrar seu afeto por alguém e como forma de proteger o meio ambiente, pois tudo que se encontra na lista será usufruido</p>


            </main>

        </div>

    </section>
    
    <section class="bg-w">
    
        <div class="max-w">
        
            <div class="col-60">
                
                <ul class="lista-item">
                
                    <li>
                    
                        <div class="col-100 bg-w item-cima">
                            
                            
                            <div class="float-left">
                                
                                
                        
                                <label class="container">Corsa Sedan 2014
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            
                            </div>
                            
                            <div class="float-right">
                                
                                <a href="#" class="link-sec">Ver Foto</a>
                                
                            </div>
                        
                        </div>
                        
                        <div class="col-100 bg-g item-baixo">
                            
                            <div class="float-left">
                        
                                <label for="quant1">Quant.: </label>

                                99
                                
                            </div>
                            
                            <div class="float-right">
                            
                                <label for="onde1">Onde: </label>

                                Lojas Americanas
                                
                            </div>
                        
                        </div>
                        
                    </li>
                    
                    <li>
                    
                        <div class="col-100 bg-w item-cima">
                            
                            
                            <div class="float-left">
                                
                                
                        
                                <label class="container">Calculadora do seninha
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                            
                            </div>
                            
                            <div class="float-right">
                                
                                <a href="#" class="link-sec">Ver Foto</a>
                                
                            </div>
                        
                        </div>
                        
                        <div class="col-100 bg-g item-baixo">
                            
                            <div class="float-left">
                        
                                <label for="quant1">Quant.: </label>

                                99
                                
                            </div>
                            
                            <div class="float-right">
                            
                                <label for="onde1">Onde: </label>

                                Lojas Americanas
                                
                            </div>
                        
                        </div>
                        
                    </li>
                    
                    <li>
                    
                        <div class="col-100 bg-w item-cima">
                            
                            
                            <div class="float-left">
                                
                                <label class="container">
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                    <input type="text" placeholder="Nome do item">
                                </label>
                            
                            </div>
                            
                            <div class="float-right">
                                
                                <label for="foto" class="link-sec">Foto</label><input type="file" accept="image/*" id="foto">
                                
                                <a href="#" class="excluir middle"><img src="assets/img/icon-fechar.png" title="Excluir item"></a>
                                
                            </div>
                        
                        </div>
                        
                        <div class="col-100 bg-g item-baixo">
                            
                            <div class="float-left">
                        
                                <label for="quant1">Quant.: </label>

                                <input type="number" id="quant1" placeholder="0">
                                
                            </div>
                        
                        </div>
                        
                    </li>
                
                </ul>
                
                <a href="#" class="add-item center uppercase col-100">Adicionar item fora da lista</a>
            
            </div>
            
            <div class="col-40">
                
                <a href="#" class="btn-prim center">Finalizar</a>
            
            </div>
        
        </div>
    
    </section>

    <script src="assets/js/jquery-3.2.1.min.js"></script>
    <script src="assets/js/funcoes.js"></script>

</body>

</html>
