<!DOCTYPE HTML>
<html lang="pt-BR">

<head>

    <title>Needlist - Monte listas para qualquer evento</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="description" content="Olá, essa é uma lista de desejos. Ela surgiu da vontade de facilitar a escolha de algo para demonstrar seu afeto por alguém e como forma de proteger o meio ambiente, pois tudo que se encontra na lista será usufruido." />
    <meta name="keywords" content="lista, evento, presente" />
    <meta name="author" content="Needlist" />

    <meta property="og:title" content="Needlist - Monte listas para qualquer evento">
    <meta property="og:url" content="http://www.needlist.com.br/">
    <meta property="og:description" content="Olá, essa é uma lista de desejos. Ela surgiu da vontade de facilitar a escolha de algo para demonstrar seu afeto por alguém e como forma de proteger o meio ambiente, pois tudo que se encontra na lista será usufruido.">
    <meta property="og:type" content="article">
    <meta property="og:locale" content="pt_BR">
    <meta property="og:image" content="http://www.needlist.com.br/img/capa-site.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="581">
    <meta property="og:image:height" content="300">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="shortcut icon" href="assets/img/favicon.png" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,900|Playfair+Display:400i,900i" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href='assets/css/grid-ramon.css' />
    <link rel="stylesheet" type="text/css" href='assets/css/estilo.css' />

</head>

<body>

    <nav class="fixed nav-act">

        <div class="max-w">

            <ul class="col-100">

                <li class="float-left"><a href="index.php" class="logo scroll">needlist</a></li>
                <li class="float-right"><a href="#" class="menu-item scroll">Minhas listas</a></li>

            </ul>

        </div>

    </nav>
    
    <hr>

    <section class="interna bg-w">
    
        <div class="max-w">
        
            <div class="col-60">
            
                <a href="#" class="link-prim">Nova lista em branco</a>
                
                <select class="uppercase bg-w">
                
                    <option>Nova lista a partir de modelo</option>
                    <option>Lista de casamento</option>
                    <option>Lista de debutante</option>
                    <option>Lista de chá de fraldas</option>
                
                </select>
                
                <form>
                    
                    <input type="text" class="input-titulo" placeholder="Nome da lista">
                    <input type="text" class="input-desc" placeholder="Escreva aqui a finalidade da lista, informando sobre data, horário e local do evento">
                    <label for="capa" class="link-sec">Escolher uma foto de capa</label><input type="file" accept="image/*" id="capa"><img src="assets/img/icon-ok.png" class="icon-ok">
                
                </form>
                
                <ul class="lista-item">
                
                    <li>
                    
                        <div class="col-100 bg-w item-cima">
                            
                            
                            <div class="float-left">
                                
                                <label class="container">
                                    <input type="checkbox" disabled>
                                    <span class="checkmark"></span>
                                    <input type="text" placeholder="Nome do item">
                                </label>
                            
                            </div>
                            
                            <div class="float-right">
                                
                                <label for="foto" class="link-sec">Foto</label><input type="file" accept="image/*" id="foto">
                                
                                <a href="#" class="excluir middle"><img src="assets/img/icon-fechar.png" title="Excluir item"></a>
                                
                            </div>
                        
                        </div>
                        
                        <div class="col-100 bg-g item-baixo">
                            
                            <div class="float-left">
                        
                                <label>Quant.: <input type="number" placeholder="0"></label>
                                
                            </div>
                            
                            <div class="float-right">
                            
                                <label>Onde: <input type="text" placeholder="Ajude a achar o item"></label>
                                
                            </div>
                        
                        </div>
                        
                    </li>
                
                </ul>
                
                <ul class="lista-item">
                
                    <li>
                    
                        <div class="col-100 bg-w item-cima">
                            
                            
                            <div class="float-left">
                                
                                <label class="container">
                                    <input type="checkbox" disabled>
                                    <span class="checkmark chk"></span>
                                    <input type="text" value="Item já selecionado" disabled>
                                </label>
                            
                            </div>
                            
                            <div class="float-right">
                                
                                <label for="foto" class="link-sec">Foto</label><input type="file" accept="image/*" id="foto">
                                
                                <a href="#" class="excluir middle"><img src="assets/img/icon-fechar.png" title="Excluir item"></a>
                                
                            </div>
                        
                        </div>
                        
                        <div class="col-100 bg-g item-baixo">
                            
                            <div class="float-left">
                        
                                <label>Quant.: <input type="number" placeholder="0" disabled></label>
                                
                            </div>
                            
                            <div class="float-right">
                            
                                <label>Onde: <input type="text" placeholder="Ajude a achar o item" disabled></label>
                                
                            </div>
                        
                        </div>
                        
                    </li>
                
                </ul>
                
                <a href="#" class="add-item center uppercase col-100">Adicionar item</a>
            
            </div>
            
            <div class="col-40">
            
                <a href="#" class="link-prim">Salvar</a>
                <a href="visualizar.php" class="link-sec">Visualizar</a>
                
                <a href="gerenciador.php" class="btn-prim center">Compartilhar</a>
            
            </div>
        
        </div>
    
    </section>

    <script src="assets/js/jquery-3.2.1.min.js"></script>
    <script src="assets/js/funcoes.js"></script>

</body>

</html>
